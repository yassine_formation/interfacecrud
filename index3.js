
let selectedRow = null;

function onFormSubmit() {
    let formData = readFormData();
    if(selectedRow == null) {
        insertNewRecord(formData);
    } else {
        updateNewRecord(formData);
    }
    resetForm();
    
}



function readFormData() {
    let formData = {};
    formData['firstname'] = document.getElementById('firstname').value;
    formData['lastname'] = document.getElementById('lastname').value;
    formData['birthdate'] = document.getElementById('birthdate').value;
    formData['age'] = ageCount();
    formData['numberId'] = document.getElementById('numberId').value;
    return formData;
}


function insertNewRecord(data) {
    let table = document.getElementById('storeList').getElementsByTagName('tbody')[0];
    let newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.firstname;
    cell2 = newRow.insertCell(1);
    cell2.innerHTML = data.lastname;
    cell3 = newRow.insertCell(2);
    cell3.innerHTML = data.birthdate;
    cell4 = newRow.insertCell(3);
    cell4.innerHTML = data.age;
    cell5 = newRow.insertCell(4);
    cell5.innerHTML = data.numberId;
    cell6 = newRow.insertCell(5);
    cell6.innerHTML = `<button onClick = "onEdit(this)">Edit</button> <button onClick = "onDelete(this)">Delete</button>`
}


function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById('firstname').value = selectedRow.cells[0].innerHTML;
    document.getElementById('lastname').value = selectedRow.cells[1].innerHTML;
    document.getElementById('birthdate').value = selectedRow.cells[2].innerHTML;
    document.getElementById('numberId').value = selectedRow.cells[3].innerHTML;
}

function updateNewRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.firstname;
    selectedRow.cells[1].innerHTML = formData.lastname;
    selectedRow.cells[2].innerHTML = formData.birthdate;
    selectedRow.cells[4].innerHTML = formData.numberId;
}


function onDelete(td) {
    if(confirm('Voulez-vous supprimer cette element ?')) {
        row = td.parentElement.parentElement;
        document.getElementById('storeList').deleteRow(row.rowIndex);
        resetForm();
    }
}

function resetForm() {
    document.getElementById('firstname').value = "";
    document.getElementById('lastname').value = "";
    document.getElementById('birthdate').value ="";
    document.getElementById('numberId').value = "";
    selectedRow = null;
}

function ageCount() {
    let now = new Date();//obtenir la date du jour
    let currentY = now.getFullYear();// extraction de l'année
    let currentX = now.getMonth();//extraction du mois

    let dobget = document.getElementById('birthdate').value;
    let dob = new Date(dobget);
    let prevY = dob.getFullYear();
    let prevX = dob.getMonth();

    let ageY = currentY - prevY;
    let ageM = Math.abs(currentX - prevX);

    let age = ageY + ' ans ' + ageM + ' mois';
    return age;
}

/* 


function ordreCroissant(data) {
    let checkbox = document.querySelector("input[name=croissant");
    let newTable;

    checkbox.addEventListener('change', function() {
       if(this.checked) {
           // trier ordre croissant            
           
           
           // let tableSorted = newTable.sort();
          
       }
       else {
           console.log('no')
       }
    });
    console.log(newTable);
   }

*/